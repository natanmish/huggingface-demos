# https://huggingface.co/blog/getting-started-habana

# 1 - Create EC2 instance, see https://huggingface.co/blog/getting-started-habana

# 2 - On instance

docker pull \
vault.habana.ai/gaudi-docker/1.4.0/ubuntu20.04/habanalabs/pytorch-installer-1.10.2:1.4.0-442

docker run -it \
--runtime=habana \
-e HABANA_VISIBLE_DEVICES=all \
-e OMPI_MCA_btl_vader_single_copy_mechanism=none \
--cap-add=sys_nice \
--net=host \
--ipc=host vault.habana.ai/gaudi-docker/1.4.0/ubuntu20.04/habanalabs/pytorch-installer-1.10.2:1.4.0-442

# 3 - Inside container

git clone https://github.com/huggingface/optimum-habana.git
cd optimum-habana
python setup.py install

# 4 - Train

# 1 HPU
python train_habana.py

# Distributed training with 8 HPUs
python /optimum-habana/examples/gaudi_spawn.py --world_size 8 --use_mpi 03_train_habana.py
